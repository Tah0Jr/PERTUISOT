//Pertuisot
//Mathéo
//22/10/19
//Mission1 
//QUESTION 4


/*
Objectif :  Compléter le programme de la question 3. Le programme doit initialiser une nouvelle
donnée : le nom du produit concerné en demandant la valeur à l’utilisateur. Ensuite, le programme
affiche directement le prix TTC à payer de ce produit. N’oubliez pas d’afficher un message explicite à
l’utilisateur pour qu’il sache quoi saisir. 
*/
import java.util.Scanner ;//on récuper l'outil scanner 
public class Mission1Q4{
        public static void main (String[] args){
                Scanner sc = new Scanner(System.in);//on prépare l'outil scanner
				System.out.println("Quel est le nom du produit");//affiche le message
				String produit = sc.nextLine();//on recuper la valeur saisi par l'utilisateur 
				System.out.println("Quel est le prix Ht?");//affiche le message
				double prixHT = sc.nextInt();//on récuper la 2eme valeur saisi par l'utilisateur
                System.out.println("Quelle est la quantit\u00e9 achet\u00e9e ?");//affiche le message
                int quantite = sc.nextInt();//on recuper la 3eme valeur saisi 
		        double TVA=20;//ajoute la variable TVA
                double prixTTC;// ajoute la variable prixTTC
	        	prixTTC=quantite*prixHT*(1+TVA/100);//calcule le prix total TTC
                System.out.println("Le prix TTC du produit " +produit +" est : " +prixTTC);//Affiche le message + le nom du produit+le prix TTC
		}
}
