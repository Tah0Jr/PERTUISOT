//Pertuisot
//Mathéo
//22/10/19
//Mission2
//QUESTION1


/*
Objectif :  Compléter le programme de la Mission 1 en accordant une remise de 10 % sur le prix TTC
pour tout achat de plus de 100 pièces. Ecrire le programme calculant le prix TTC en fonction de la
quantité, du prix HT et du taux de TVA. 
*/
import java.util.Scanner ;//on récuper l'outil scanner 
public class Mission2Q1{
        public static void main (String[] args){
                Scanner sc = new Scanner(System.in);//on prépare l'outil scanner
				System.out.println("Quel est le nom du produit");//affiche le message
				String produit= sc.nextLine();//on recuper la valeur saisi par l'utilisateur 
				System.out.println("Quel est le prix HT?");//affiche le message
				double prixHT = sc.nextInt();//on récuper la 2eme valeur saisi par l'utilisateur
                System.out.println("Quelle est la quantit\u00e9 achet\u00e9e ?");//affiche le message
                int quantite = sc.nextInt();//on recuper la 3eme valeur saisi 
				if (quantite>100) System.out.println("Vous disposez d'une remise de 10%"); // affiche le message
		        double TVA=20;//rajoute la variable TVA
                double prixTTC;//rajoute la variable prixTTC
	        	prixTTC=quantite*prixHT*(1+TVA/100);//calcule le prix total TTC
				double reduc= prixTTC/10;//rajoute la variable reduc
				double prixTTC10= prixTTC-reduc;//rajoute la variable prixTTC10
                System.out.println("Le prix TTC de votre panier de " +produit +"est : " +prixTTC);//Affiche le message + le nom du produit+le prix TTC
				if (quantite>100);//crée une alternative si le resultat entrer par l'utilisateur est supérieur à 100
				System.out.println("Grace a votre reduction, le prix TTC de votre panier de " +produit +"est: " +prixTTC10);//affiche le message contenant la reduction si la quantité est supérieur a 100
		}
}

