# SI2 Mission 2: **Reseau IP avec Filius**

Etape 1:
==

## *1) A quel réseau IP appartient chaque machine*
 1) Elles appartiennent au meme reseau.
## *2) Peuvent-elles potentiellement communiquer entre elles*
 2) Oui, car elles appartiennent au meme réseau.
## *3) Peuvent-elles dans les conditions physiques présentées ci-dessus communiquer entre elles?*
 3) Non, car elles ne sont pas relié entre elle.
## *4) Quelle commande sur le terminal Filius permet de voir la configuration IP d’une STA*
 4) ipconfig
## *5)Quelle commande permet de tester que les messages IP sont bien transmise entre deux machines?*
 5) ping + l'adresse ip du destinataire



Etape 2:
==

## *1) Quel média de communication communication a été ajouté ? Expliquer brièvement son fonctionnement ? Expliquer brièvement son fonctionnement* 
 1)C'est un switch qui a été ajouter, un outil qui permet de communiquer entre plusieurs machines sur un même réseaux
## *2) Quel autre média aurait pu le remplacer (inexistant sur Filius)? En quoi est-il différent du premier?*
 2)Un hub aurait pue remplacer le switch. Sa manière de fonctionner est différentex ,
 car il renvoie toutes les informations qu'il recoit a toutes les machines sur le réseau
## *3)